#!/bin/bash
# {{ ansible_managed }}

set -e

NOW=$(date +"%Y.%m.%d-%H.%M.%S")
MYSQL_SOURCE_DB_NAME="{{ import_source_db_name }}"
MYSQL_SOURCE_CONF_FILE_PATH="$HOME/.mysql-import-source.cnf"
MYSQL_SOURCE_DUMP_FILE_PATH="{{ db_io_tmp_dir }}/remote-$MYSQL_SOURCE_DB_NAME-$NOW.sql"

if ! [ -e $MYSQL_SOURCE_CONF_FILE_PATH ]; then
    >&2 echo "$MYSQL_SOURCE_CONF_FILE_PATH does not exist"
    exit 1
fi

MYSQL_SOURCE_HOST=$(awk -F "=" '/host/ {print $2}' $MYSQL_SOURCE_CONF_FILE_PATH | tr -d ' ')
MYSQL_SOURCE_PORT=$(awk -F "=" '/port/ {print $2}' $MYSQL_SOURCE_CONF_FILE_PATH | tr -d ' ')

echo -e "\n# Network conection test"
CONN_RESULT=`timeout 2 bash -c "</dev/tcp/$MYSQL_SOURCE_HOST/$MYSQL_SOURCE_PORT"; echo $?`

if [ $CONN_RESULT -ne "0" ]; then
    >&2 echo "Can not connect to MySQL at $MYSQL_SOURCE_HOST:$MYSQL_SOURCE_PORT"
    exit 1
fi

echo -e "\n# MySQL credentials test"
mysql --defaults-extra-file=$MYSQL_SOURCE_CONF_FILE_PATH -N -e "select 'Connection to MySQL server successful'"

echo -e "\n# Dumping source DB"
mysqldump \
    --defaults-extra-file=$MYSQL_SOURCE_CONF_FILE_PATH \
    --routines \
    --events \
    --triggers \
    --complete-insert \
    $MYSQL_SOURCE_DB_NAME \
    > $MYSQL_SOURCE_DUMP_FILE_PATH

import-db-from-file.sh -f $MYSQL_SOURCE_DUMP_FILE_PATH

echo -e "\n# Deleting source DB dump file"
rm $MYSQL_SOURCE_DUMP_FILE_PATH
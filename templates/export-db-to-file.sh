#!/bin/bash
# {{ ansible_managed }}

set -e

NOW=$(date +"%Y.%m.%d-%H.%M.%S")
MYSQL_DB_NAME="{{ primary_db_name }}"
MYSQL_CONF_FILE_PATH="$HOME/.mysql-primary.cnf"
MYSQL_DUMP_FILE_PATH="{{ db_io_tmp_dir }}/$MYSQL_DB_NAME-$NOW.sql"
MYSQL_DUMP_ARCHIVE_FILE_PATH="{{ db_io_export_dir }}/$MYSQL_DB_NAME.sql.gz"

echo -e "\n# Exporting DB to $MYSQL_DUMP_FILE_PATH"
mysqldump \
    --defaults-extra-file=$MYSQL_CONF_FILE_PATH \
    -h localhost \
    --routines \
    --events \
    --triggers \
    --complete-insert \
    $MYSQL_DB_NAME \
    > $MYSQL_DUMP_FILE_PATH

echo -e "\n# Archiving export file to $MYSQL_DUMP_ARCHIVE_FILE_PATH"
gzip -c $MYSQL_DUMP_FILE_PATH > $MYSQL_DUMP_ARCHIVE_FILE_PATH

echo -e "\n# Removing uncompressed sql file"
rm $MYSQL_DUMP_FILE_PATH
#!/bin/bash
# {{ ansible_managed }}

set -e

NOW=$(date +"%Y.%m.%d-%H.%M.%S")
MYSQL_DB_NAME="{{ primary_db_name }}"
MYSQL_DB_USER="{{ primary_db_user }}"
MYSQL_CONF_FILE_PATH="$HOME/.mysql-primary.cnf"
MYSQL_BACKUP_FILE_PATH="{{ db_io_backup_dir }}/$MYSQL_DB_NAME-$NOW.sql"
GUNZIPED_FILE_PATH="{{ db_io_tmp_dir }}/_$MYSQL_DB_NAME-$NOW.sql"
POST_IMPORT_SQL_DIR="{{ db_io_post_import_sql_dir }}"
INPUT_FILEPATH=""
SQL_FILEPATH=""
set +e
DOES_DB_EXIST=`mysqlshow --defaults-extra-file=$MYSQL_CONF_FILE_PATH | grep -v Wildcard | grep -o $MYSQL_DB_NAME > /dev/null 2>&1 && echo 1`
set -e

usage()
{
cat << EOF
REQUIRED ARGUMENTS:
	-f	Filepath to the sql file (can be gzipped)
EOF
}

while getopts hf: opts; do
   case ${opts} in
	h) usage
	   exit 1 ;;
	f) INPUT_FILEPATH=${OPTARG} ;;
   esac
done

if [[ -z $INPUT_FILEPATH ]]
then
     usage
     exit 1
fi

if [[ ! -r $INPUT_FILEPATH ]]
then
     echo "Filepath not a file"
     exit 1
fi

if file --mime-type "$INPUT_FILEPATH" | grep -q gzip$; then
    echo -e "\n# Gunzip sql file"   
    gunzip -c $INPUT_FILEPATH > $GUNZIPED_FILE_PATH
    SQL_FILEPATH=$GUNZIPED_FILE_PATH
else
    SQL_FILEPATH=$INPUT_FILEPATH
fi


echo -e "\n# Changing definer"
sed -i -E "s/DEFINER=\`(.*)\`@\`localhost\`/DEFINER=\`$MYSQL_DB_USER\`@\`localhost\`/g" $SQL_FILEPATH
sed -i -E "s/DEFINER=\`(.*)\`@\`127.0.0.1\`/DEFINER=\`$MYSQL_DB_USER\`@\`localhost\`/g" $SQL_FILEPATH

if [ $DOES_DB_EXIST ]; then
    echo -e "\n# Backing up DB (just in case) to $MYSQL_BACKUP_FILE_PATH"
    mysqldump \
        --defaults-extra-file=$MYSQL_CONF_FILE_PATH \
        -h localhost \
        --routines \
        --events \
        --triggers \
        --complete-insert \
        $MYSQL_DB_NAME \
        > $MYSQL_BACKUP_FILE_PATH
fi


if [ $DOES_DB_EXIST ]; then
    echo -e "\n# Dropping DB"
    mysql \
        --defaults-extra-file=$MYSQL_CONF_FILE_PATH \
        -h localhost \
        -e "DROP DATABASE $MYSQL_DB_NAME;"
fi

echo -e "\n# Creating target DB"
mysql \
    --defaults-extra-file=$MYSQL_CONF_FILE_PATH \
    -h localhost \
    -e "CREATE DATABASE $MYSQL_DB_NAME;"


echo -e "\n# Importing to target DB"
mysql \
    --defaults-extra-file=$MYSQL_CONF_FILE_PATH \
    -h localhost \
    $MYSQL_DB_NAME \
    < $SQL_FILEPATH


if [[ -d $POST_IMPORT_SQL_DIR && -n "$(ls -A $POST_IMPORT_SQL_DIR)" ]]; then
    echo -e "\n# Running post import sql"
    for POST_IMPORT_SQL_FILE in $POST_IMPORT_SQL_DIR/*; do
        echo "Running $POST_IMPORT_SQL_FILE"
        mysql \
            --defaults-extra-file=$MYSQL_CONF_FILE_PATH \
            -h localhost \
            $MYSQL_DB_NAME \
            < $POST_IMPORT_SQL_FILE
    done
fi


if file --mime-type "$INPUT_FILEPATH" | grep -q gzip$; then
    echo -e "\n# Deleting gunzipped file"
    rm $GUNZIPED_FILE_PATH
fi
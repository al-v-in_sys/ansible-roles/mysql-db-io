# MySQL DB IO - Ansible role

Role to create simple scripts to import and export MySQL database in an opinionated way. The primary database information is set in the ansible variables. Can also set the details for a secondary database (local or remote) that can be imported from.

## Use cases
- import from remote server, eg production server to dev server
- create db backup file
- restore from db backup file

### `import-db-from-file.sh`
Will import to primary database from given sql file (`-f` argument).

- if DB exists will be backed up before being overwritten
- if DB does not exist it will be created
- will accept sql or gzipped sql files
- post import modifications can be applied by sql files set in `db_io_post_import_sql_dir` directory (e.g. to change the domain in the database)

### `export-db-to-file.sh`
Will export (dump) the primary DB to gzipped sql file in directory set in ansible variables.

### `import-db-from-server.sh`
Import from secondary server and database (local or remote). Details for source database set in ansible variables `import_source_*`

You can give a local dev server access to a remote MySQL server (through the firewall etc) or you can use port forwarding. Setup port forwarding on your host machine via: `ssh -NL 0.0.0.0:33061:localhost:3306 remote-machine`, then set the `import_source_db_host/port`  to `[host-machine]:33601`.

### TODO
- multiple sets of databases